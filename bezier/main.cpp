// tester.cpp

#include <iostream>
#include <cstdlib>

#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#include <math.h>

using std::cout;
using std::endl;

using std::rand;

//------------------------------------------------
void my_init();
void my_display_func();
void my_reshape_func( int w, int h );
void my_keyboard_func( unsigned char key, int x, int y );
void my_mouse_func( int button, int state, int x, int y );

//------------------------------------------------
const int window_width  = 150; // Number of columns of pixels.
const int window_height = 40; // Number of rows.

const int image_width = window_width; // Also in pixels.
const int image_height = window_height;

// 3 bytes (r, g, b) for each pixel. Think of the pixels being numbered
// as in this example:
//
//
//  48,49,50 (4,0)	 51,52,53 (4,1)	 54,55,56 (4,2)	 57,58,59 (4,3)
//  36,37,38 (3,0)	 39,40,41 (3,1)	 42,43,44 (3,2)	 45,46,47 (3,3)
//  24,25,26 (2,0)	 27,28,29 (2,1)	 30,31,32 (2,2)	 33,34,35 (2,3)
//  12,13,14 (1,0)	 15,16,17 (1,1)	 18,19,20 (1,2)	 21,22,23 (1,3)
//   0,1,2   (0,0)	   3,4,5  (0,1)	  6,7,8   (0,2)	  9,10,11 (0,3)
//
// The numbers separated by commas are indices into the pixels array.
// The numbers in parenthesis are row and column indices, respectively.
//
// Note that, in the above example:
// image_width  = 4
// image_height = 5
//
// So, to index into the pixel at (3, 2) -- 42,43,44:
// y = 3 (row = 3)
// x = 2 (column = 2)
//
// (row * image_width * 3) + (col * 3) + 0 = (2 * 3) + (3 * 4 * 3) + 0 = 6 + 36 = 42
// (row * image_width * 3) + (col * 3) + 1 = 43
// (row * image_width * 3) + (col * 3) + 2 = 44
unsigned char pixels[ image_width * image_height * 3];

//------------------------------------------------
int main( int argc, char * argv[] )
{
	 glutInit( &argc, argv );
	 glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
	 glutInitWindowSize( window_width, window_height );
	 glutInitWindowPosition( 50, 50 ); // x, y.
	 glutCreateWindow( "drawing pixels" );

	 my_init();

	 glutDisplayFunc( my_display_func );
	 glutReshapeFunc( my_reshape_func );
	 glutKeyboardFunc( my_keyboard_func );
	 glutMouseFunc( my_mouse_func );

	 glutMainLoop();
	 return 0;
}

//------------------------------------------------
void my_init()
{
	 glClearColor( 0.0, 0.0, 0.0, 0.0 );
}

//------------------------------------------------
void my_display_func() {
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	int x[4];
	int y[4];

	x[0] = 0;
	x[1] = 30;
	x[2] = 104;
	x[3] = 149;

	y[0] = 5;
	y[1] = 5;
	y[2] = 35;
	y[3] = 35;

	int i;
	double t;

	int p[image_width][image_height];

	for (int col = 0; col < image_width; col++) {
		for (int row = 0; row < image_height; row++) {
			p[row][col] = 0;
		}
	}

	for (int z = 0; z < image_width * image_height * 3; z++) {
		pixels[z] = 0;
	}


	for (t = 0.0; t < 1.0; t += 0.0005) {
		double xt = pow (1-t, 3) * x[0] + 3 * t * pow (1-t, 2) * x[1] + 3 * pow (t, 2) * (1-t) * x[2] + pow (t, 3) * x[3];

		double yt = pow (1-t, 3) * y[0] + 3 * t * pow (1-t, 2) * y[1] + 3 * pow (t, 2) * (1-t) * y[2] + pow (t, 3) * y[3];

		int xx = (int)floor(xt);
		int yy = (int)floor(yt);

		if (xx == 46 and yy == 4) {
			printf("sdfsdfsdfs");
		}

		p[xx][yy] = 1;
	}

	for (int row = 0; row < image_height; row++) {
		for (int col = 0; col < image_width; col++) {
			if (p[col][row] == 1) {
				//printf("%d", row);

				pixels[ ( row * image_width * 3 ) + ( col * 3 ) + 0 ] = 0; // R
				pixels[ ( row * image_width * 3 ) + ( col * 3 ) + 1 ] = 255; // G
				pixels[ ( row * image_width * 3 ) + ( col * 3 ) + 2 ] = 0; // B
			} else {
				//printf(" ");
			}
		}

		printf("\n");
	}

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glRasterPos2i(0, 0);
	glDrawPixels(image_width, image_height, GL_RGB, GL_UNSIGNED_BYTE, pixels);

	glutSwapBuffers();
}

//------------------------------------------------
void my_reshape_func( int w, int h )
{
	 glViewport( 0, 0, w, h );

	 glMatrixMode( GL_PROJECTION );
	 glLoadIdentity();

	 // Clipping volume x-y coords match up with pixel window coords:
	 glOrtho( 0.0, static_cast<double>( w ),
			  0.0, static_cast<double>( h ),
			  -1.0, 1.0 );

	 glMatrixMode( GL_MODELVIEW );
	 glLoadIdentity();

	 glutPostRedisplay();
}

//-----------------------------------------------------------------
// Note, only one event per key-press. No keyup *and* keydown here.
void my_keyboard_func( unsigned char key, int x, int y )
{
	 switch ( key )
	 {
		 case 'q':
			 cout << "Bye!" << endl;
			 exit( 0 ); // How do I simply "exit the glutMainLoop()?"
			 break;

		 case 'h':
			 cout << "Hit q to quit." << endl;
			 break;

		 default:
			 cout << "You press a key but it doesn't seem to do anything..." << endl;
			 break;
	 }
}

//---------------------------------------------------------
void my_mouse_func( int button, int state, int x, int y )
{
	 switch ( button )
	 {
		 case GLUT_LEFT_BUTTON:
			 if ( state == GLUT_DOWN )
			 {
				 cout << "Please refrain from clicking the mouse button\n";
				 cout << "Thanks, ---the management." << endl;
			 }
			 break;
	 }
}
