#include <EEPROM.h>
#include "storage.h"


// void setup() {
// 	Serial.begin(115200);

// 	// Storage * storage1 = new Storage();

// 	// storage1->settings->spark_dwell_time = 99;
// 	// storage1->points->add(100, 200);

// 	// storage1->write();



// 	Storage * storage2 = new Storage();

// 	storage2->read();


// 	Serial.println(storage2->points->get(0).RPM);
// }

// void loop() {

// }




#define BACKLIGHT_OFF 0x0
#define BACKLIGHT_ON 0x7

#define ENABLE_LCD false

#if ENABLE_LCD
	#include <Wire.h>
	#include <Adafruit/Adafruit_MCP23017.h>
	#include <Adafruit/Adafruit_RGBLCDShield.h>

	Adafruit_RGBLCDShield lcd = Adafruit_RGBLCDShield();
#endif

Storage * storage;
long * point_map;

// unsigned long	g_last_ui_update = 0;
// float			g_last_ui_pulse_count = 0;
// unsigned long	g_last_pulse = 0;
// float			g_manual_delay = 1;
// int				g_RPM;
// float			g_one_degree_time = 0;
// float			g_total_delay = 0;
// float			g_status = 0;
// unsigned long	g_last_degree_time = 0; // this is used to keep a tiny running average of pulse time so as to smooth out the fluctuation of firing time at low speed.

// float			g_points[MAX_RPM / CURVE_GRANULARITY];

// void  setup();
// void  pulse();
// void  loop();
// void  fire_spark();
// float get_approximate_point();
long * map_points(Points * points);
// void  directPinMode(int pinNumber, int mode);
// void  directDigitalWrite(int pinNumber, int mode);

void setup() {
 	Serial.begin(115200);

 	Serial.println("setup()");

	#if ENABLE_LCD
		lcd.begin(16, 2);

		lcd.setBacklight(BACKLIGHT_ON);

		lcd.setCursor(0, 0);
		lcd.print("Martian");

		lcd.setCursor(0, 1);
		lcd.print("Industries");

		delay(100);
	#endif

	storage = new Storage();

	point_map = map_points(storage->points);

// 	if (plot_points(coordinates)) {
// 		directPinMode(SPARK_PIN, OUTPUT);

// 		attachInterrupt(HALL_PIN, pulse, CHANGE);

// 		Serial.println("CDI Started");

// 		#if ENABLE_LCD
// 			lcd.clear();
// 			lcd.setCursor(0, 0);
// 			lcd.print("CDI Started!");
// 			lcd.setCursor(0, 1);
// 			lcd.print("GO GO GO!");

// 			delay(100);

// 			lcd.clear();
// 		#endif
// 	}
}

void loop() {
// 	unsigned long ui_update_duration;
// 	unsigned long now_time;

// 	now_time = millis();

// 	ui_update_duration = now_time - g_last_ui_update;

// 	if (ui_update_duration > UI_UPDATE_FREQUENCY) {
// 		if ((now_time - g_last_pulse) >  UI_UPDATE_FREQUENCY) {
// 			g_RPM = 0;
// 		}

// 		g_manual_delay = MANUAL_ADJUST_FREEDOM * ((float)(analogRead(MANUAL_DELAY_PIN) + 1) / 1024) * MANUAL_ADJUST_FREEDOM;

// 		#if ENABLE_LCD
// 		lcd.setCursor(0, 0);
// 		lcd.print("RPM:       ");
// 		lcd.setCursor(4, 0);
// 		lcd.print(g_RPM); // + " Delay: " + g_total_delay);

// 		lcd.setCursor(0, 1);
// 		lcd.print("Delay:       ");
// 		lcd.setCursor(6, 1);
// 		lcd.print(g_total_delay / g_one_degree_time); // + " Delay: " + g_total_delay);
// 		#endif

// 		g_last_ui_update = now_time;
// 		g_last_ui_pulse_count = 0;
// 	}
}

// void pulse() {
// 	static int pulses = 0;
// 	//unsigned long start_time = micros();
// 	unsigned long now_time = micros();
// 	unsigned int manual_delay = 0;
// 	float delay_fix_time = 0;
// 	float pulse_rotation_time = 0;
// 	float curve_delay = 0;

// 	pulses++;
// 	g_last_ui_pulse_count++;

// 	if ((pulses == MAGNET_COUNT) & (g_last_pulse != 0)) {
// 		pulse_rotation_time = now_time - g_last_pulse;

// 		g_last_pulse = now_time;

// 		g_one_degree_time = (float)pulse_rotation_time / (float)DEGREES_PER_MAGNET;

// 		g_RPM = (int)(60000000 / (float)(g_one_degree_time * 360));

// 		//////////////////////////////////////////////////////////////////////
// 		//////////////////////////////////////////////////////////////////////

// 		curve_delay = get_approximate_point() * g_one_degree_time;

// 		manual_delay = (int)((g_one_degree_time / MANUAL_ADJUST_FREEDOM) * g_manual_delay) + 1;

// 		delay_fix_time = (1 - (g_RPM / (float)MAX_RPM)) * (g_one_degree_time * 20.0);

// 		g_status = delay_fix_time;

// 		g_total_delay = 1 + curve_delay + manual_delay + delay_fix_time + (g_one_degree_time - ((g_one_degree_time + g_last_degree_time)/2));

// 		delayMicroseconds(g_total_delay);

// 		fire_spark();

// 		pulses = 0;

// 		g_last_degree_time = g_one_degree_time;
// 	} else {
// 		g_last_pulse = now_time;
// 	}
// }

// void fire_spark() {
// 	directDigitalWrite(SPARK_PIN, HIGH);

// 	delayMicroseconds(SPARK_DWELL_TIME);

// 	directDigitalWrite(SPARK_PIN, LOW);
// }

// long * map_points(Points * points) {
// 	int coordinate_count = 0;

// 	int delta_counter;

// 	float delta;
// 	float t, T;
// 	int x;
// 	float y;

// 	for (int count = 0; count < points->count; count++) {
// 		Coordinate current, next;

// 		current = points->get(count);

// 		if (count == (points->count - 1)) {
// 			next = points->get(count);
// 		} else {
// 			next = points->get(count + 1);
// 		}

// 		int curve_effect = (int)storage->settings->max_rpm / storage->settings->curve_granularity;

// 		delta = 1.0 / curve_effect;

// 		t = 0;

// 		for (delta_counter = 0; delta_counter < curve_effect; delta_counter++) {
// 			t += delta;

// 			T = 1 - t;

// 			int curve_strength = (int)((next.x - current.x) / 4);

// 			x = current.x * T * T * T + 3 * t * T * T * (current.x + curve_strength) + 3 * t * t * T * (next.x - curve_strength) + t * t * t * next.x;
// 			y = current.y * T * T * T + 3 * t * T * T * current.y + 3 * t * t * T * next.y + t * t * t * next.y;

// 			g_points[x] = y;
// 		}
// 	}

// 	return 0;
// }

long * map_points(Points * points) {
	Serial.println("map_points()");

	float t, T, y, delta;
	int x, curve_strength, delta_counter;

	int map_size = (storage->settings->max_rpm / storage->settings->curve_granularity) + 1;

	long * map = (long *) malloc(sizeof(long) * map_size);

	// Temporarily clean up the map for testing
	for (int i = 0; i < map_size; i++) { map[i] = 0; }

	/*
	Since our final timing map array will have 150 cells in it since we have 15k set as the max RPM and are dealing with
	100 RPM intervals, this will put the point defined at 8000 RPM in the 80th cell.
	*/

	for (int i = 0; i < points->count; i++) {
		Points::Coordinate curr_coord = points->get(i);

		map[(int)(curr_coord.RPM / storage->settings->curve_granularity)] = curr_coord.degrees;
	}

	delta = 1.0 / (int)(map_size);

	for (int i = 0; i < map_size; i++) {
		int current, next;

		current = map[i];

		if (i == (map_size - 1)) {
			next = map[i];
		} else {
			next = map[i + 1];
		}

		t = 0;

		for (delta_counter = 0; delta_counter < map_size; delta_counter++) {
			t += delta;

			T = 1 - t;

			curve_strength = (int)((next.x - current.x) / 4);

			x = current.x * T * T * T + 3 * t * T * T * (current.x + curve_strength) + 3 * t * t * T * (next.x - curve_strength) + t * t * t * next.x;
			y = current.y * T * T * T + 3 * t * T * T * current.y + 3 * t * t * T * next.y + t * t * t * next.y;

			g_points[x] = y;
		}
	}

	return map;
}









// float get_approximate_point() {
// 	float current = g_points[(int)(g_RPM / CURVE_GRANULARITY)];
// 	float next =  g_points[(int)(g_RPM / CURVE_GRANULARITY) + 1];

// 	return current + ((next - current) * ((g_RPM % CURVE_GRANULARITY) / CURVE_GRANULARITY));
// }

// void directPinMode(int pinNumber, int mode) {
// 	volatile unsigned char* target;

// 	if (pinNumber < 8) {
// 		target = &DDRD;
// 	} else {
// 		target = &DDRB;

// 		pinNumber -= 8;
// 	}

// 	if (mode) {
// 		*target |= 1 << pinNumber;
// 	} else {
// 		*target &= ~(1 << pinNumber);
// 	}
// }

// void directDigitalWrite(int pinNumber, int mode) {
// 	volatile unsigned char* target;

// 	if (pinNumber < 8) {
// 		target = &PORTD;
// 	} else {
// 		target = &PORTB;

// 		pinNumber -= 8;
// 	}

// 	if (mode) {
// 		*target |= 1 << pinNumber;
// 	} else {
// 		*target &= ~(1 << pinNumber);
// 	}
// }
