#include <EEPROM.h>
#include <Arduino.h>

template <class T> void eeprom_write(int &eeprom_position, const T& value) {
	const byte* curr_byte = (const byte*)(const void*)&value;

	unsigned int i;

	for (i = 0; i < sizeof(value); i++) {
		EEPROM.write(eeprom_position++, *curr_byte++);
	}

	eeprom_position += i;
}

template <class T> void eeprom_read(int &eeprom_position, T& value) {
	byte* curr_byte = (byte*)(void*)&value;

	unsigned int i;

	for (i = 0; i < sizeof(value); i++) {
		*curr_byte++ = EEPROM.read(eeprom_position++);
	}

	eeprom_position += i;
}

class Points {
	public:
		int count;

		struct Coordinate {
			unsigned int RPM;
			unsigned int degrees;
		};

		Points() {
			this->initialize();
		}

		Points(int &eeprom_position) {
			this->initialize();

			this->read(eeprom_position);
		}

		void add(unsigned int RPM, unsigned int degrees) {
			Coordinate new_coordinate = {RPM, degrees};

			this->add(new_coordinate);
		}

		void add(Coordinate new_coordinate) {
			if (is_full()) {
				resize(this->size + 1);
			}

			this->points[this->count++] = new_coordinate;
		}

		Coordinate get(int position) {
			return this->points[position];
		}

		void write(int &eeprom_position) {
			eeprom_write(eeprom_position, this->count);

			for (int i = 0; i < this->count; i++) {
				eeprom_write(eeprom_position, this->points[i]);
			}
		}

		void read(int &eeprom_position) {
			int stored_count;

			eeprom_read(eeprom_position, stored_count);

			for (int i = 0; i < stored_count; i++) {
			 	Coordinate new_coordinate;

				eeprom_read(eeprom_position, new_coordinate);

				this->add(new_coordinate);
			}
		}

		void clear() {
			free(this->points);

			points = (Coordinate *) malloc(sizeof(Coordinate));

			this->count = 0;
			this->size = 1;
		}

	private:
		Coordinate * points;

		int size;

		void initialize() {
			this->points = (Coordinate *) malloc(sizeof(Coordinate));

			this->size = 1;
			this->count = 0;
		}

		bool is_full() {
			return this->size == this->count;
		}

		void resize(int new_size) {
			this->points = (Coordinate *) realloc(this->points, sizeof(Coordinate) * new_size);

			this->size = new_size;
		}
};

#define DEFAULT_BEZIER_WEIGHT 40
#define DEFAULT_CURVE_GRANULARITY 100
#define DEFAULT_DEGREES_PER_MAGNET 360 / DEFAULT_MAGNET_COUNT
#define DEFAULT_HALL_PIN 0
#define DEFAULT_INITIAL_ADVANCE 0
#define DEFAULT_MAGNET_COUNT 2
#define DEFAULT_MANUAL_ADJUST_FREEDOM 20
#define DEFAULT_MANUAL_DELAY_PIN 0 // Analogue pin
#define DEFAULT_MAX_RPM 15000 // MAX_RPM must be evenly divisible by DEFAULT_CURVE_GRANULARITY
#define DEFAULT_SPARK_DWELL_TIME 20
#define DEFAULT_SPARK_PIN 8
#define DEFAULT_UI_UPDATE_FREQUENCY 800

class Settings {
	public:
		int bezier_weight;
		int curve_granularity;
		int degrees_per_magnet;
		int hall_pin;
		int initial_advance;
		int magnet_count;
		int manual_adjust_freedom;
		int manual_delay_pin;
		int max_rpm;
		int spark_dwell_time;
		int spark_pin;
		int ui_update_frequency;

		Settings() {
			this->initialize();
		}

		Settings(int &eeprom_position) {
			this->initialize();

			this->read(eeprom_position);
		}

		void write(int &eeprom_position) {
			eeprom_write(eeprom_position, this);
		}

		void read(int &eeprom_position) {
			eeprom_write(eeprom_position, this);
		}

	private:
		void initialize() {
			this->bezier_weight = DEFAULT_BEZIER_WEIGHT;
			this->curve_granularity = DEFAULT_CURVE_GRANULARITY;
			this->degrees_per_magnet = DEFAULT_DEGREES_PER_MAGNET;
			this->hall_pin = DEFAULT_HALL_PIN; // Interrupt pin 0 = digital 2 on the UNO
			this->initial_advance = DEFAULT_INITIAL_ADVANCE;
			this->magnet_count = DEFAULT_MAGNET_COUNT;
			this->manual_adjust_freedom = DEFAULT_MANUAL_ADJUST_FREEDOM;
			this->manual_delay_pin = DEFAULT_MANUAL_DELAY_PIN;
			this->max_rpm = DEFAULT_MAX_RPM;
			this->spark_dwell_time = DEFAULT_SPARK_DWELL_TIME;
			this->spark_pin = DEFAULT_SPARK_PIN;
			this->ui_update_frequency = DEFAULT_UI_UPDATE_FREQUENCY;
		}
};

class Storage {
	public:
		Points * points;
		Settings * settings;

		Storage() {
			this->clear();

			int eeprom_position = 0;

			this->settings = new Settings(eeprom_position);
			this->points = new Points(eeprom_position);

			if (this->points->count == 0) {
				this->points->add(0, 10);
				this->points->add(this->settings->max_rpm, 20);

				this->write();
			}
		}

		void write() {
			int eeprom_position = 0;

			this->settings->write(eeprom_position);
			this->points->write(eeprom_position);
		}

		void read() {
			int eeprom_position = 0;

			this->settings->read(eeprom_position);
			this->points->read(eeprom_position);
		}

		void clear() {
			for (int i = 0; i < 512; i++) {
				EEPROM.write(i, 0);
			}
		}
};
